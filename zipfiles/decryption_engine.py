import cryptomath, math, platform, os

class decryptionEngine(object):
  def decrypt_affine(self, key, message):
    symbols = """abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 !"#$%&'()*+,-./:;<=>?@[\]^_`{|}~"""
    keyA = key // len(symbols)
    keyB = key % len(symbols)
    plaintext = ''
    modInverseOfKeyA = cryptomath.findModInverse(keyA, len(symbols))
    for symbol in message:
      if symbol in symbols:
        symIndex = symbols.find(symbol)
        plaintext += symbols[(symIndex - keyB) * modInverseOfKeyA % len(symbols)]
      else:
        plaintext += symbol
    return plaintext

  def decrypt_transposition(self, key, message):
    numOfColumns = math.ceil(len(message) / key)
    numOfRows = key
    numOfShadedBoxes = (numOfColumns * numOfRows) - len(message)
    plaintext = [''] * numOfColumns
    col = 0
    row = 0
    for symbol in message:
      plaintext[col] += symbol
      col += 1
      if (col == numOfColumns) or (col == numOfColumns - 1 and row >= numOfRows - numOfShadedBoxes):
        col = 0
        row += 1
    return ''.join(plaintext)

  def decrypt_vigenere(self, key, message):
    letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    translated = []
    keyIndex = 0
    key = key.upper()
    for symbol in message:
      num = letters.find(symbol.upper())
      if num != -1:
        num -= letters.find(key[keyIndex])
        num %= len(letters)
        if symbol.isupper():
          translated.append(letters[num])
        elif symbol.islower():
          translated.append(letters[num].lower())
        keyIndex += 1
        if keyIndex == len(key):
          keyIndex = 0
      else:
        translated.append(symbol)
    return ''.join(translated)

  def decrypt(self, keys, message, counter = 16):
    if counter != 0:
      message = self.decrypt(keys, message, counter - 1)
    # print('%s: %s' % (counter, message)) # Prints each recursion level
    return self.decrypt_affine(keys['affine'], self.decrypt_transposition(keys['transposition'], self.decrypt_vigenere(keys['vigenere'], message)))

key_file = open('keyfile').read().splitlines()
keys = {
  'affine': int(key_file[0]),
  'vigenere': key_file[1],
  'transposition': int(key_file[2])
}
decryptor = decryptionEngine()

# TODO: ADD THIS FUNCTIONALITY
admin = False
# -

if(platform.system() == 'Windows'):
  root = 'C:/' if admin else 'C:/Users/'
else:
  root = '/' if admin else '/home/dann/workspace/python/assignment/filesystem/'

for dirname, dirnames, filenames in os.walk(root):
  for filename in filenames:    
    name, ext = os.path.splitext(filename)
    if ext == '.0wn3d':
      # open file and read contents
      file = open(os.path.join(dirname, filename))
      content = file.read()
      file.close()

      # decrypt contents
      P = decryptor.decrypt(keys, content)

      # write encrypted text to file
      output_file = open(os.path.join(dirname, filename), 'w')
      output_file.write(P)
      output_file.close()