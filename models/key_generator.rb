class KeyGenerator
  SYMBOLS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 !\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~"
  RAND_GEN = Random.new

  def generate_key
    return "#{generate_affine_key}#{generate_vigenere_key}#{generate_transposition_key}"
  end

  private
  # Return the greatest common divisor of x and y using Euclid's Algorithm
  def gcd(x, y)
    while x != 0 do
      x, y = y % x, x
    end
    return y
  end

  def generate_random_key
    while true do
      key_one = RAND_GEN.rand(2..SYMBOLS.length)
      key_two = RAND_GEN.rand(2...SYMBOLS.length)
      return (key_one * SYMBOLS.length + key_two) if gcd(key_one, SYMBOLS.length) == 1
    end
  end

  def check_affine_key(key)
    key_one = (key / SYMBOLS.length).floor
    key_two = key % SYMBOLS.length
    if gcd(key_one, SYMBOLS.length) != 1
      return false
    else
      return true
    end
  end

  def generate_affine_key
    key = generate_random_key
    until check_affine_key(key) do
      key = generate_random_key
    end
    return key  
  end

  def generate_transposition_key
    return RAND_GEN.rand(5..SYMBOLS.length)
  end

  def generate_vigenere_key
    key = [''] * RAND_GEN.rand(20..40)
    key.map! { |element| element = SYMBOLS[RAND_GEN.rand(0..25)] }
    return key.join
  end
end


# Tests
# Ran over a million times and not one key failed the check. Redundant code?
# invalid_keys = []
# 250000.times do 
#   key = generate_random_key
#   invalid_keys << key unless check_affine_key(key)
# end
# p invalid_keys

# p generate_vigenere_key

# 10000.times { puts generate_key }