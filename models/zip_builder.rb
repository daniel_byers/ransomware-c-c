require 'zip'

class ZipBuilder  
  def initialize(root_folder, data)
    @root_folder = root_folder
    @data = data
  end

  def build
    generate_keyfile
    file = pack
    delete_keyfile
    return file
  end

  private

  def generate_keyfile
    key = @data['cipher_key']
    affine = key.match(/^\d{3,4}/)[0]
    vigenere = key.match(/[a-z]{20,40}/)[0]
    transposition = key.match(/\d{1,2}$/)[0]
    File.open(File.join(@root_folder, 'zipfiles', 'keyfile'), "w") do |file|
      file.write("#{affine}\n#{vigenere}\n#{transposition}")
    end
  end

  def pack
    decrypt_file = "#{@root_folder}/zipfiles/#{@data['uid_hash']}.zip"
    File.delete(decrypt_file) if File.exist?(decrypt_file)
    Zip::File.open(decrypt_file, Zip::File::CREATE) do |zipfile|
      ['keyfile', 'decryption_engine.py', 'cryptomath.py'].each do |filename|
        zipfile.add(filename, File.join(@root_folder, 'zipfiles', filename))
      end
    end
    return decrypt_file
  end

  def delete_keyfile
    File.delete(File.join(@root_folder, 'zipfiles', 'keyfile'))
  end
end